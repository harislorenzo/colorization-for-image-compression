#!/usr/bin/env python2

import caffe_pb2
import math
import google.protobuf as pb
from copy import deepcopy

# Load the mean values and the averages from the dataset created.

# Our value of averages are 123 for Y, 128 for Cb and 131 for Cr

caffe_base_path = '../../'

data_train_str = '''
  name: "data"
  type: "ImageData"
  top: "data"
  top: "label"
  include {
    phase: TRAIN
  }
  transform_param {
    mirror: true
    crop_size: 224
    mean_value: 122.7577
    scale: 1	
  }
  image_data_param {
    source: "''' + caffe_base_path + '''examples/colorization/training_paths.txt" 
    batch_size: 8
    shuffle: true
  }
'''

data_test_str = '''
  name: "data"
  type: "ImageData"
  top: "data"
  top: "label"
  include {
    phase: TEST
  }
  transform_param {
    mirror: false
    crop_size: 224
    mean_value: 122.7577
    scale: 1
  }
  image_data_param {
    source: "''' + caffe_base_path + '''examples/colorization/validation_paths.txt"
    batch_size: 2
  }
'''



conv_str = '''
  name: "conv_"
  type: "Convolution"
  bottom: "conv_"
  top: "conv_"
  param {
    lr_mult: 1
    decay_mult: 1  
  } 
  convolution_param {
    bias_term: false
    num_output: 64
    kernel_size: 3
    pad: 1
    stride: 1
    weight_filler {
      type: "msra"
      variance_norm: FAN_OUT
    }
  }
'''

relu_str = '''
  name: "relu_"
  type: "ReLU"
  bottom: "conv_"
  top: "conv_"
'''

norm_str = '''
  name: "norm_"
  type: "BatchNorm"
  bottom: "conv_"
  top: "conv_"
  param { lr_mult: 0 } 
  param { lr_mult: 0 } 
  param { lr_mult: 0 } 
  batch_norm_param {
  }	
'''

scale_str = '''
  bottom: "conv_"
  top: "conv_"
  name: "scale_"
  type: "Scale"
  param {
    lr_mult: 1
    decay_mult: 0  
  }
  param {
    lr_mult: 1
    decay_mult: 0  
  }
  scale_param {
	bias_term: true
	axis:1
  }
'''


pool_str = '''
  name: "pool_"
  type: "Pooling"
  bottom: "conv_"
  top: "pool_"
  pooling_param {
    pool: AVE
    kernel_size: 8
    stride: 1
  }
'''

fc_str = '''
  name: "fc_"
  type: "InnerProduct"
  bottom: "pool_"
  top: "fc_"
  param {
    lr_mult: 1
    decay_mult: 1  
  }
  param {
    lr_mult: 1
    decay_mult: 0  
  }
  inner_product_param {
    num_output: 10
    weight_filler {
      type: "gaussian"
      std: 0.01
    }
    bias_filler {
      type: "constant"
    }
  }
'''

elem_str = '''
  name: "shortcut_"
  type: "Eltwise"
  bottom: "relu_"
  bottom: "relu_"
  top: "elem_"
  eltwise_param { operation: SUM }
'''

_conv = caffe_pb2.LayerParameter()
pb.text_format.Merge(conv_str, _conv)
_norm = caffe_pb2.LayerParameter()
pb.text_format.Merge(norm_str, _norm)
_relu = caffe_pb2.LayerParameter()
pb.text_format.Merge(relu_str, _relu)
_elem = caffe_pb2.LayerParameter()
pb.text_format.Merge(elem_str, _elem)
_pool = caffe_pb2.LayerParameter()
pb.text_format.Merge(pool_str, _pool)
_fc = caffe_pb2.LayerParameter()
pb.text_format.Merge(fc_str, _fc)
_scale = caffe_pb2.LayerParameter()
pb.text_format.Merge(scale_str, _scale)



data_train = caffe_pb2.LayerParameter()
pb.text_format.Merge(data_train_str, data_train)
data_test = caffe_pb2.LayerParameter()
pb.text_format.Merge(data_test_str, data_test)

final_conv_outs = 256
semifinal_conv_outs = 128
quarter_final_outs = 64

BRANCH_FACTOR = 5

concat_init_str = '''
      name: "concat"'''
concat_branches = ''
for a_k in range(0,BRANCH_FACTOR):
    concat_branches = concat_branches + '\n      bottom: "in"'

concat_end_str ='''
      top: "joint_loss"
      type: "Concat"
      concat_param{
        axis: 0
      }
    '''
concat_str = concat_init_str + concat_branches + concat_end_str

_concat = caffe_pb2.LayerParameter()
pb.text_format.Merge(concat_str, _concat)
concat = _concat

def add_k_branches(bottom_layer_name, cur_k_index):
# THIS IS THE MULTI-MODAL PREDICTION PART

    conv_semifinal = deepcopy(_conv)
    conv_semifinal.name = 'conv_semifinal' + '_' + str(cur_k_index)
    conv_semifinal.bottom[0] = bottom_layer_name
    conv_semifinal.top[0] = 'conv_semifinal' + '_' + str(cur_k_index)
    conv_semifinal.convolution_param.num_output = 16	# Indicating nthe number of channels in the output
    conv_semifinal.convolution_param.kernel_size[0] = 3
    conv_semifinal.convolution_param.weight_filler.type='msra'
    conv_semifinal.param[0].decay_mult = 0.0;
    conv_semifinal.convolution_param.bias_term = True
    conv_semifinal.convolution_param.pad[0]= 1

    relu = deepcopy(_relu)
    relu.name = 'relu_semifinal' + '_' + str(cur_k_index)
    relu.top[0] = 'conv_semifinal' + '_' + str(cur_k_index)
    relu.bottom[0] = 'conv_semifinal' + '_' + str(cur_k_index)

    layers.extend([conv_semifinal, relu])

    conv_final = deepcopy(_conv)
    conv_final.name = 'conv_final' + '_' + str(cur_k_index)
    conv_final.bottom[0] = 'conv_semifinal' + '_' + str(cur_k_index)
    conv_final.top[0] = 'conv_final'+ '_' + str(cur_k_index)
    conv_final.convolution_param.num_output = 2	# Indicating nthe number of channels in the output
    conv_final.convolution_param.kernel_size[0] = 3
    conv_final.convolution_param.weight_filler.type='gaussian'
    conv_final.convolution_param.weight_filler.std=0.01
    conv_final.convolution_param.bias_filler.type='twobias'
    conv_final.convolution_param.bias_filler.value=128
    conv_final.convolution_param.bias_filler.value_two = 131
    conv_final.param[0].decay_mult = 0.0;
    conv_final.convolution_param.bias_term = True
    conv_final.convolution_param.pad[0]= 1

    relu = deepcopy(_relu)
    relu.name = 'relu_final' + '_' + str(cur_k_index)
    relu.top[0] = 'conv_final' + '_' + str(cur_k_index)
    relu.bottom[0] = 'conv_final' + '_' + str(cur_k_index)

    layers.extend([conv_final, relu])







for n_const in [9]:

    layers = []
    layers.extend([data_train, data_test])

    layer_idx = 0
    layer_str = str(layer_idx)

    conv = deepcopy(_conv)
    conv.name = 'conv_' + layer_str
    conv.top[0] = 'conv_' + layer_str
    conv.bottom[0] = 'data'
    conv.convolution_param.kernel_size[0] = 7
    conv.convolution_param.pad[0] = 3
    conv.convolution_param.stride[0] = 2

    norm = deepcopy(_norm)
    norm.name = 'norm_' + layer_str
    norm.top[0] = 'conv_' + layer_str
    norm.bottom[0] = 'conv_' + layer_str

    scale = deepcopy(_scale)	
    scale.name = 'scale_' + layer_str
    scale.top[0] = 'conv_' + layer_str
    scale.bottom[0] = 'conv_' + layer_str

    relu = deepcopy(_relu)
    relu.name = 'relu_' + layer_str
    relu.top[0] = 'conv_' + layer_str
    relu.bottom[0] = 'conv_' + layer_str

    pool = deepcopy(_pool)
    pool.name = 'pool1'
    pool.pooling_param.kernel_size = 3
    pool.pooling_param.stride = 2
    pool.pooling_param.pool = 0 # 0 - MAX, 1- AVE
    pool.bottom[0] = 'conv_' + layer_str
    pool.top[0] = 'pool1'

    layers.extend([conv, norm, scale, relu, pool])

#layers.extend([conv, norm, relu])

# Haris modifications : 
# Projection layers are introduced with some adhock criteria
# Correct way is to verify if output size has decreased prior to introducing the elementwise operation
# This brings up the question of when is size of the output not matching

# Can additionally add 3,5,9,and so on to n_const to generate networks of different depths
    reps = 0

    for output_size in [64, 128, 256]:
	
	if output_size == 64:
	    reps = 3
	if output_size == 128:
	    reps = 3
	if output_size == 256:
	    reps = 3
	if output_size == 512:
	    reps = 3

        for i in range(reps):

	    print str(i) + " is " + 'conv_' + layer_str
            # first branch is same size with pad 0, kernel size 1
            layer_idx += 1
            layer_str = str(layer_idx)

            conv = deepcopy(_conv)
            conv.name = 'conv_' + layer_str
            conv.top[0] = 'conv_' + layer_str
	    conv.bottom[0] = 'elem_' + str(layer_idx-1)
	    
	    if output_size == 64 and i == 0 :
		conv.bottom[0] = 'pool1'
            
            conv.convolution_param.num_output = output_size
	    conv.convolution_param.kernel_size[0] = 1
	    conv.convolution_param.stride[0] = 1

	    if 	i == 0 and (output_size > 64):
		conv.convolution_param.stride[0] = 2	

	    conv.convolution_param.pad[0] = 0

# PARTIALLY RESPONSIBLE FOR PROJECTIONS
#            if layer_idx == (2*n_const+1) or layer_idx == (4*n_const+1):
#		conv.convolution_param.stride[0] = 2	

            norm = deepcopy(_norm)
            norm.name = 'norm_' + layer_str
            norm.top[0] = 'conv_' + layer_str
            norm.bottom[0] = 'conv_' + layer_str

            scale = deepcopy(_scale)	
            scale.name = 'scale_' + layer_str
            scale.top[0] = 'conv_' + layer_str
            scale.bottom[0] = 'conv_' + layer_str

            relu = deepcopy(_relu)
            relu.name = 'relu_' + layer_str
            relu.top[0] = 'conv_' + layer_str
            relu.bottom[0] = 'conv_' + layer_str

            layers.extend([conv, norm, scale, relu])
#            layers.extend([conv, norm, relu])

            #################
            # 2

            layer_idx += 1
            layer_str = str(layer_idx)

            conv = deepcopy(_conv)
            conv.name = 'conv_' + layer_str
            conv.top[0] = 'conv_' + layer_str
            conv.bottom[0] = 'conv_' + str(layer_idx-1)
            conv.convolution_param.num_output = output_size

            norm = deepcopy(_norm)
            norm.name = 'norm_' + layer_str
            norm.top[0] = 'conv_' + layer_str
            norm.bottom[0] = 'conv_' + layer_str

            scale = deepcopy(_scale)	
            scale.name = 'scale_' + layer_str
            scale.top[0] = 'conv_' + layer_str
            scale.bottom[0] = 'conv_' + layer_str

            relu = deepcopy(_relu)
            relu.name = 'relu_' + layer_str
            relu.top[0] = 'conv_' + layer_str
            relu.bottom[0] = 'conv_' + layer_str

            layers.extend([conv, norm, scale, relu])

            #################
            # 3

            layer_idx += 1
            layer_str = str(layer_idx)

            conv = deepcopy(_conv)
            conv.name = 'conv_' + layer_str
            conv.top[0] = 'conv_' + layer_str
            conv.bottom[0] = 'conv_' + str(layer_idx-1)
            conv.convolution_param.num_output = output_size*4
	    conv.convolution_param.kernel_size[0] = 1
	    conv.convolution_param.stride[0] = 1
	    conv.convolution_param.pad[0] = 0

            norm = deepcopy(_norm)
            norm.name = 'norm_' + layer_str
            norm.top[0] = 'conv_' + layer_str
            norm.bottom[0] = 'conv_' + layer_str

            scale = deepcopy(_scale)	
            scale.name = 'scale_' + layer_str
            scale.top[0] = 'conv_' + layer_str
            scale.bottom[0] = 'conv_' + layer_str

	    layers.extend([conv, norm, scale])		
            ##################
            # shortcut


            # short cut is either with projection or without projection
            # if the if statement is true then we want to create a shortcut with a projection
            # if the if statement is NOT true than we want to create a shortcut without a projection
	
            if layer_idx in [3, 12, 21]:

                conv = deepcopy(_conv)
                conv.name = 'conv_proj_' + str(layer_idx-2)
                conv.top[0] = 'conv_proj_' + str(layer_idx-2)
                conv.bottom[0] = 'elem_' + str(layer_idx-3)
		
		if layer_idx == 3:
		    conv.bottom[0] = 'pool1'

		conv.convolution_param.weight_filler.type="identity";
                conv.convolution_param.num_output = output_size*4
                conv.convolution_param.kernel_size[0] = 1# Kernel size of 1 
                conv.convolution_param.stride[0] = 2# Stride of 2 lets us downsample this
		if layer_idx == 3:
		    conv.convolution_param.stride[0] = 1# Stride of 2 lets us downsample this
                conv.convolution_param.pad[0] = 0
                conv.param[0].lr_mult = 0
		conv.param[0].decay_mult = 0

                norm = deepcopy(_norm)
                norm.name = 'norm_' + str(layer_idx-2) + '_proj'
                norm.top[0] = 'conv_proj_' + str(layer_idx-2)
                norm.bottom[0] = 'conv_proj_' + str(layer_idx-2)

                scale = deepcopy(_scale)	
                scale.name = 'scale_' + str(layer_idx-2) + '_proj'
                scale.top[0] = 'conv_proj_' + str(layer_idx-2)
                scale.bottom[0] = 'conv_proj_' + str(layer_idx-2)

		# For shortcuts we also use the normalization and proejction layers as observed with Resnet-50
                layers.extend([conv, norm, scale])


                elem = deepcopy(_elem)
                elem.name = 'elem_' + layer_str
                elem.top[0] = 'elem_' + layer_str
                elem.bottom[0] = 'conv_' + layer_str
                elem.bottom[1] = 'conv_proj_' + str(layer_idx-2)

                layers.extend([elem])


            else:
                elem = deepcopy(_elem)
                elem.name = 'elem_' + layer_str
                elem.top[0] = 'elem_' + layer_str
                elem.bottom[0] = 'conv_' + layer_str
                elem.bottom[1] = 'elem_' + str(layer_idx-3)

                layers.extend([elem])


            relu = deepcopy(_relu)
            relu.name = 'relu_' + layer_str
            relu.top[0] = 'elem_' + layer_str
            relu.bottom[0] = 'elem_' + layer_str

            layers.extend([relu])

    layer_str_prev = str(layer_idx)    

    layer_idx = 1 + layer_idx
    layer_str = str(layer_idx)

    # Adding upsampling prior to comparison with loss
    upsample_str = '''
      name: "upsample"
      type: "Deconvolution"
      bottom: "conv_"
      top: "upsample_"
      param {
        lr_mult: 0
        decay_mult: 0
      }
      convolution_param {
        kernel_size: 4
        stride: 2
        num_output: 2
	group: 2
        pad: 1
        weight_filler{
	  type: "bilinear"
	}
	bias_term: false
      }'''     	

    conv_final = deepcopy(_conv)
    conv_final.name = 'conv_' + layer_str
    conv_final.bottom[0] = 'elem_' + layer_str_prev
    conv_final.top[0] = 'conv_' + layer_str
    conv_final.convolution_param.num_output = 256	# Indicating nthe number of channels in the output
    conv_final.convolution_param.kernel_size[0] = 3
    conv_final.convolution_param.weight_filler.type='msra'
    conv_final.param[0].decay_mult = 0.0;
    conv_final.convolution_param.bias_term = False
    conv_final.convolution_param.pad[0]= 1

    norm = deepcopy(_norm)
    norm.name = 'norm_' + layer_str
    norm.top[0] = 'conv_' + layer_str
    norm.bottom[0] = 'conv_' + layer_str

    scale = deepcopy(_scale)
    scale.name = 'scale_' + layer_str
    scale.top[0] = 'conv_' + layer_str
    scale.bottom[0] = 'conv_' + layer_str	

    layers.extend([conv_final, norm, scale])

    _upsample = caffe_pb2.LayerParameter()
    pb.text_format.Merge(upsample_str, _upsample)
    upsample = deepcopy(_upsample)
    upsample.name = 'upsample_' + layer_str
    upsample.bottom[0] = 'conv_' + layer_str
    upsample.top[0] = 'upsample_' + layer_str
    upsample.convolution_param.num_output = 256
    upsample.convolution_param.group = 256
    upsample.convolution_param.kernel_size[0] = 8# special layer being upsampled by a factor of 8
    upsample.convolution_param.stride[0] = 4
    upsample.convolution_param.pad[0] = 2

    layers.extend([upsample])

    upsample = deepcopy(_upsample)
    upsample.name = 'upsample_' + layer_str + '_old'
    upsample.bottom[0] = 'elem_' + str(9)
    upsample.top[0] = 'upsample_' + layer_str + '_old'
    upsample.convolution_param.num_output = 256
    upsample.convolution_param.group = 256

    layers.extend([upsample])

    elem = deepcopy(_elem)
    elem.name = 'elem_' + layer_str
    elem.top[0] = 'elem_' + layer_str
    elem.bottom[0] = 'upsample_' + layer_str
    elem.bottom[1] =  'upsample_' + layer_str + '_old'

    relu = deepcopy(_relu)
    relu.name = 'relu_' + layer_str
    relu.top[0] = 'elem_' + layer_str
    relu.bottom[0] = 'elem_' + layer_str

    layers.extend([elem,relu])

    layer_idx = 1 + layer_idx
    layer_str_prev = str(layer_idx-1)
    layer_str = str(layer_idx)

    conv_final = deepcopy(_conv)
    conv_final.name = 'conv_' + layer_str
    conv_final.bottom[0] = 'elem_' + layer_str_prev
    conv_final.top[0] = 'conv_' + layer_str
    conv_final.convolution_param.num_output = 64	# Indicating nthe number of channels in the output
    conv_final.convolution_param.kernel_size[0] = 3
    conv_final.convolution_param.weight_filler.type='msra'
    conv_final.param[0].decay_mult = 0.0;
    conv_final.convolution_param.bias_term = False
    conv_final.convolution_param.pad[0]= 1

    norm = deepcopy(_norm)
    norm.name = 'norm_' + layer_str
    norm.top[0] = 'conv_' + layer_str
    norm.bottom[0] = 'conv_' + layer_str

    scale = deepcopy(_scale)
    scale.name = 'scale_' + layer_str
    scale.top[0] = 'conv_' + layer_str
    scale.bottom[0] = 'conv_' + layer_str	

    layers.extend([conv_final, norm, scale])

    upsample = deepcopy(_upsample)
    upsample.name = 'upsample_' + layer_str
    upsample.bottom[0] = 'conv_' + layer_str
    upsample.top[0] = 'upsample_' + layer_str
    upsample.convolution_param.num_output = 64
    upsample.convolution_param.group = 64

    layers.extend([upsample])

    relu = deepcopy(_relu)
    relu.name = 'elem_' + layer_str
    relu.top[0] = 'elem_' + layer_str
    relu.bottom[0] = 'upsample_' + layer_str

    layers.extend([relu])

# THis is where the K way branches will come in
    for a_k in range(0,BRANCH_FACTOR):
	add_k_branches( 'elem_' + layer_str, a_k)


    for a_k in range(0,BRANCH_FACTOR):
	concat.bottom[a_k] = 'conv_final' +'_' + str(a_k)
    
    layers.extend([concat])

    loss_str = '''
      name: "loss"
      type: "EuclideanLoss"
      bottom: "fc_"
      bottom: "label"
      top: "loss"
    '''
    _loss = caffe_pb2.LayerParameter()
    pb.text_format.Merge(loss_str, _loss)
    loss = _loss
    loss.bottom[0] = 'joint_loss' 

    layers.extend([loss])


    net = caffe_pb2.NetParameter()
    net.name = "resnet_imagenet_color_50_B" +str(BRANCH_FACTOR) + "2"
    net.layer.extend(layers)
    open(net.name+'.prototxt', 'w').write(str(net))
