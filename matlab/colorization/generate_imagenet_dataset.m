clc
clear all
close all

% Generating Imagenet Dataset

% Create Image net data
path_to_caffe = '~/Documents/compressionRepository/';

% Dataset creation can be speeded up significantly if one has access to a
% matlab cluster.
% If so, please call the function "CreateImageNetBatch" as a batch job
% batch(cluster_object,CreateImageNetBatch,3,{batch_start, batch_end, a_batch,...
%                                             output_path, training_file_path,...
%                                             use_base_path, prepend_path, is_test});

% .txt file containing as rows full paths to training files
% if full paths not available specify use_base_path and prepend_path
use_base_path = 1;
prepend_path = '/path_to_dataset_root/';

% this is the path where you want to store your final dataset
% please make sure this path is accessible from your GPU's or wherever you
% want to perform training on.
output_base_path = '';

training_file_path = [path_to_caffe '/matlab/colorization/main_real_train.txt'];
validation_file_path = [path_to_caffe '/matlab/colorization/main_real_val.txt'];
benchmark_file_path = [path_to_caffe '/matlab/colorization/main_real_benchmark.txt'];   % TESTING SET

batch_size = 5000; % batch_size*3 files created (if exceed 5000 access speed decreases)

all_tr_data = importdata(training_file_path);
total_tr = size( all_tr_data.data,1 );


total_tr_batches = ceil( total_tr / batch_size );
batch_start = 1;
data_type = 'TR';
is_test = 0;
% Each batch writes its individual file
% Once all individual files have been generated
% The code below merges them all.
mkdir('./TEMP')
for a_batch  = 1: total_tr_batches
  
   batch_end = min( batch_start + batch_size - 1, total_tr );      
   CreateImageNetBatch(batch_start, batch_end, a_batch,...
                       output_path, training_file_path,...
                       use_base_path, prepend_path, data_type, is_test);
                   
   fprintf('Finished writing Training Batch %d of %d\n',a_batch, total_tr_batches);
   batch_start = batch_end+1;
    
end
fprintf('Finished Creating Training Dataset\n')

% Write training dataset file 
file_handle = fopen([path_to_caffe 'examples/colorization/training_paths.txt'],'w');
for a_batch = 1: total_tr_batches
    load(['./TEMP/TR_Batch_' sprintf('%04d',a_batch) '.mat'])
    total_paths = size(all_names,1);
    for a_name = 1: total_paths
        fprintf(file_handle,'%s %s\n',all_names{a_name,1},all_names{a_name,2});
    end
end
fclose(file_handle);

%% Now for Validation

all_val_data = importdata(validation_file_path);
total_val = size( all_val_data.data,1 );

total_val_batches = ceil( total_val / batch_size );
batch_start = 1;

data_type = 'VAL';
is_test = 0;
% Each batch writes its individual file
% Once all individual files have been generated
% The code below merges them all.
mkdir('./TEMP')
for a_batch  = 1: total_val_batches
  
   batch_end = min( batch_start + batch_size - 1, total_val );      
   CreateImageNetBatch(batch_start, batch_end, a_batch,...
                       output_path, validation_file_path,...
                       use_base_path, prepend_path, data_type, is_test);
                   
   fprintf('Finished writing Validation Batch %d of %d\n',a_batch, total_val_batches);
   batch_start = batch_end+1;
    
end
fprintf('Finished Creating Validation Dataset\n')


% Write validation dataset file 
file_handle = fopen([path_to_caffe 'examples/colorization/validation_paths.txt'],'w');
for a_batch = 1: total_val_batches
    load(['./TEMP/VAL_Batch_' sprintf('%04d',a_batch) '.mat'])
    total_paths = size(all_names,1);
    for a_name = 1: total_paths
        fprintf(file_handle,'%s %s\n',all_names{a_name,1},all_names{a_name,2});
    end
end
fclose(file_handle);

%% For Benchmark

all_te_data = importdata(benchmark_file_path);
total_te = size( all_te_data.data,1 );

total_te_batches = ceil( total_te / batch_size );
batch_start = 1;


data_type = 'TE';
is_test = 1;
% Each batch writes its individual file
% Once all individual files have been generated
% The code below merges them all.
mkdir('./TEMP')
for a_batch  = 1: total_te_batches
  
   batch_end = min( batch_start + batch_size - 1, total_val );      
   CreateImageNetBatch(batch_start, batch_end, a_batch,...
                       output_path, benchmark_file_path,...
                       use_base_path, prepend_path, data_type, is_test);
                   
   fprintf('Finished writing Test Batch %d of %d\n',a_batch, total_te_batches);
   batch_start = batch_end+1;
    
end
fprintf('Finished Creating Testing/Benchmark Dataset\n')


% Write testing/benchmark dataset file 
file_handle = fopen([path_to_caffe 'examples/colorization/benchmark_paths.txt'],'w');
for a_batch = 1: total_te_batches
    load(['./TEMP/TE_Batch_' sprintf('%04d',a_batch) '.mat'])
    total_paths = size(all_names,1);
    for a_name = 1: total_paths
        fprintf(file_handle,'%s %s\n',all_names{a_name,1},all_names{a_name,2});
    end
end
fclose(file_handle);