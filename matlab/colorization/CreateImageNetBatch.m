function CreateImageNetBatch(batch_start, batch_end, a_batch,...
                             output_path, training_file_path,...
                             use_base_path, prepend_path, data_type, is_test)

% This file checks if this batch from the dataset has been created and if
% so it skips this part.
% This allows for fast resume on the long dataset creation process.

if exist(['TEMP/TR_Batch_' sprintf('%04d',a_batch) '.mat'],'file') == 0
    
    all_tr_data = importdata(training_file_path);
    
    all_names = cell(batch_end-batch_start+1,2);
    
    if exist([output_path '/BATCH_' num2str(a_batch)],'dir') == 0
        mkdir( [output_path '/BATCH_' num2str(a_batch)] )
    end
    
    complete_ouput_path = [output_path '/BATCH_' num2str(a_batch) '/'];
    
    a_file = 1;
    for a_tr = batch_start:batch_end
        
        
        if use_base_path == 0
            cur_full_file_path = all_tr_data.textdata{a_tr};
        else
            cur_full_file_path = [prepend_path all_tr_data.textdata{a_tr}];
        end
        
        [path, name, extension] = fileparts(cur_full_file_path);
        
        
        cur_img = imread( cur_full_file_path );
        
        cc_img = rgb2ycbcr( cur_img );
        
        % CHECK SIZE
        if size(cc_img,1) ~= 256 || size( cc_img,2) ~= 256
           cc_img = imresize( cc_img,[256 256],'bilinear'); 
        end
        
        
        y_img = cc_img(:,:,1);
        cb_img = cc_img(:,:,2);
        cr_img = cc_img(:,:,3);
        
        
        imwrite(y_img, [complete_ouput_path '/' name '_Y' extension],'jpg');
        if is_test ~= 1
            imwrite(cb_img, [complete_ouput_path '/' name '_Cb' extension],'jpg');
            imwrite(cr_img, [complete_ouput_path '/' name '_Cr' extension],'jpg');
        end
        
        % Add to list
        all_names{a_file,1} = [complete_ouput_path '/' name '_Y' extension];
        all_names{a_file,2} = [complete_ouput_path '/' name ];
        
        if mod(a_file,100) == 0
            fprintf('Processed Images %d\n',a_file)
        end
        
        a_file = a_file+1;
    end
    
    save(['TEMP/' data_type '_Batch_' sprintf('%04d',a_batch) '.mat'],'all_names');
    
end