#include <vector>
#include <iostream>
#include "caffe/layers/euclidean_loss_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void EuclideanLossLayer<Dtype>::Forward_gpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) { 
 
  // Add code here to implement the computation of probabilties   
  int count = bottom[0]->count();  

  const Dtype* input_data = bottom[0]->cpu_data();
  const Dtype* target = bottom[1]->cpu_data();

  Dtype* gpu_diff = diff_.mutable_cpu_data();

  int total_channels = bottom[1]->channels();
  int total_height = bottom[1]->height();
  int total_width = bottom[1]->width();
  int total_examples = bottom[1]->num();
 
  int total_samples = bottom[0]->num();
  int total_branches = total_samples / total_examples;

  int spatial_dim = total_height*total_width;

//  printf("Channels : %d\n",total_channels); 
//  printf("Examples : %d\n",total_examples); 

  int index_vec = -1;
  int label_index_vec = -1;
  int est_prediction = -1;
  int target_pixel = -1;

  int single_branch_data = total_channels*spatial_dim*total_examples;

  


  int min_err = 100000;
  int min_choice = -1;	
  int corrected_diff = 20000;
  int cur_diff = 0;

  double counter_samples[5] = {0};

  for ( int an_example = 0; an_example < total_examples; an_example ++ )
 {    

        for (int a_sp_loc = 0; a_sp_loc< spatial_dim; a_sp_loc++ )
        {		
		
		min_err = 256*256*2;
		min_choice = -1;	
        	corrected_diff = min_err;
		cur_diff = 0;

		for (int a_branch = 0; a_branch < total_branches; a_branch ++ ) 
    		{
			cur_diff = 0;
			for ( int a_channel = 0; a_channel < total_channels; a_channel++)
			{	   		
				index_vec = a_branch*single_branch_data + (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;
				label_index_vec = (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;

				est_prediction = (int)round( input_data[index_vec] );
				target_pixel = (int)round( target[label_index_vec] );
			
				if ( est_prediction  < 14 )
				{
					est_prediction = 14;
				}
				else if ( est_prediction > 240 )
				{
					est_prediction = 240;
				}
			
				corrected_diff = pow( est_prediction - target_pixel ,2);
				cur_diff = cur_diff + corrected_diff;
				
			}	// for loop on channels ends here

			if  ( cur_diff <= min_err  )
			{
				min_err = cur_diff;
				min_choice = a_branch;
			}
				
		} // for loop on branches ends here
		
		// replicate branch correction module with correct choice

		for (int a_branch = 0; a_branch < total_branches; a_branch ++ ) 
    		{
			if ( a_branch == min_choice )
			{

				for ( int a_channel = 0; a_channel < total_channels; a_channel++)
				{		   		
					index_vec = a_branch*single_branch_data + (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;
					label_index_vec = (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;

					est_prediction = (int)round( input_data[index_vec] );	
					target_pixel = (int)round( target[label_index_vec] );
			
					if ( est_prediction  < 14 )
					{
						est_prediction = 14;
					}
					else if ( est_prediction > 240 )
					{
						est_prediction = 240;
					}
			
					corrected_diff = est_prediction - target_pixel ;
					gpu_diff[index_vec] = corrected_diff;
			
				}	// for loop on channels ends here
			} // identified correct choice and set corresponding gradients here
			else 
			{
				for ( int a_channel = 0; a_channel < total_channels; a_channel++)
				{		   		
					index_vec = a_branch*single_branch_data + (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;				
					gpu_diff[index_vec] = 0;
			
				}	// for loop on channels ends here	
			}
			
		} // for loop on branches for setting correct gradients ends here

     	} // for loop on spatial locations ends here

 } // for loop on examples ends here

/*
  caffe_gpu_sub(
      count,
      bottom[0]->cpu_data(),
      bottom[1]->cpu_data(),
      diff_.mutable_gpu_data());
*/

//	printf("\n");
//	printf("\n");

  Dtype dot;
  caffe_gpu_dot(count, diff_.gpu_data(), diff_.gpu_data(), &dot);
  Dtype loss = dot / ( bottom[1]->num() ) / Dtype(2);

  top[0]->mutable_cpu_data()[0] = loss;

 
}

template <typename Dtype>
void EuclideanLossLayer<Dtype>::Backward_gpu(const vector<Blob<Dtype>*>& top,
    const vector<bool>& propagate_down, const vector<Blob<Dtype>*>& bottom) {
  for (int i = 0; i < 2; ++i) {

    if (propagate_down[i]) {
      const Dtype sign = (i == 0) ? 1 : -1;
//      const Dtype alpha = sign * top[0]->cpu_diff()[0] / bottom[i]->num();
      const Dtype alpha = sign * (top[0]->cpu_diff()[0] / ( bottom[i]->count() ) );

      caffe_gpu_axpby(
          bottom[i]->count(),              // count
          alpha,                              // alpha
          diff_.gpu_data(),                   // a
          Dtype(0),                           // beta
          bottom[i]->mutable_gpu_diff());  // b
    }
  }
}

INSTANTIATE_LAYER_GPU_FUNCS(EuclideanLossLayer);

}  // namespace caffe
