#include <functional>
#include <utility>
#include <vector>

#include "caffe/layers/compute_prob_layer.hpp"
#include "caffe/util/math_functions.hpp"

namespace caffe {

template <typename Dtype>
void ComputeProbabilityLayer<Dtype>::LayerSetUp(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {

}

template <typename Dtype>
void ComputeProbabilityLayer<Dtype>::Reshape(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
//  CHECK_LE(top_k_, bottom[0]->count() / bottom[1]->count())
//      << "top_k must be less than or equal to the number of classes.";

  label_axis_ =
      bottom[0]->CanonicalAxisIndex(1);

// INPUT ZERO CONTAINS THE PREDICTIONS

  vector<int> top_shape;  // Accuracy is a scalar; 0 axes.
  top_shape.push_back( bottom[1]->num() );
  top_shape.push_back( 1 );
  top_shape.push_back( bottom[0]->width() );
  top_shape.push_back( bottom[0]->height() ); 

  top[0]->Reshape(top_shape);

}

template <typename Dtype>
void ComputeProbabilityLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {


  Dtype* est_prob = top[0]->mutable_cpu_data();

  const Dtype* input_data = bottom[0]->cpu_data();
  const Dtype* target = bottom[1]->cpu_data();

  // Add code here to implement the computation of probabilties   
 
  int total_channels = bottom[0]->channels();

  int total_height = bottom[1]->height();
  int total_width = bottom[1]->width();
  int total_examples = bottom[1]->num();
  int spatial_dim = total_height*total_width;

 
  int total_samples = bottom[0]->num();
  int total_branches = total_samples / total_examples;

  int index_vec = -1;
  int label_index_vec = -1;
  int est_prediction = -1;
  int target_pixel = -1;
  int target_index_vec = -1;

  int single_branch_data = total_channels*spatial_dim*total_examples;
  
  double min_err = 100000;
  int min_choice = -1;	
  double corrected_diff = 20000;
  double cur_diff = 0;


  for ( int an_example = 0; an_example < total_examples; an_example ++ )
 {    
	//printf("At Example : %d\n",an_example);
        for (int a_sp_loc = 0; a_sp_loc< spatial_dim; a_sp_loc++ )
        {		
		//printf("At Loc : %d\n",a_sp_loc);
		min_err = 256*256*2;
		min_choice = -1;	

		for (int a_branch = 0; a_branch < total_branches; a_branch ++ ) 
    		{

			cur_diff = 0;
			for ( int a_channel = 0; a_channel < total_channels; a_channel++)
			{	   
						
				index_vec = a_branch*single_branch_data + (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;
				label_index_vec = (an_example*total_channels*spatial_dim) + (a_channel*spatial_dim) + a_sp_loc;

				est_prediction = input_data[index_vec] ;
				if ( est_prediction < 0 )
				{
					est_prediction = 0;
				}
				if (est_prediction > 255)
				{
					est_prediction = 255;
				}

				target_pixel = target[label_index_vec] ;
									
				corrected_diff = abs( est_prediction - target_pixel );
				cur_diff = cur_diff + corrected_diff;
								

			}	// for loop on channels ends here
			
			if ( cur_diff <= min_err )
			{
				min_err = cur_diff;
				min_choice = a_branch;
			}
	
		} // for loop on branches ends here
		
		target_index_vec = (an_example*spatial_dim) + a_sp_loc;
		est_prob[target_index_vec] = min_choice;

     	} // for loop on spatial locations ends here

 } // for loop on examples ends here
	 
}


INSTANTIATE_CLASS(ComputeProbabilityLayer);
REGISTER_LAYER_CLASS(ComputeProbability);

}  // namespace caffe
