#!/usr/bin/env sh

CAFFEBASEPATH=""
TOOLS=$CAFFEBASEPATH"/build/tools"

GLOG_log_dir="./LOG_TRAIN/" $TOOLS/caffe train --solver=$CAFFEBASEPATH"/examples/colorization/solver.prototxt" -gpu=0,1,2,3 --weights=$CAFFEBASEPATH"/models/colorization/model.caffemodel"



